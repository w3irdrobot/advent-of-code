function rotate(rotationDirection, currentDirection) {
  const possibleDirections = ['N', 'E', 'S', 'W'];
  const i = possibleDirections.indexOf(currentDirection);
  const newI = rotationDirection === 'R' ? i + 1 : i - 1
  const wrappedIndex = (newI + possibleDirections.length) % possibleDirections.length;

  return possibleDirections[wrappedIndex];
}

function move(number, currentDirection, currentLocation) {
  const curLoc = currentLocation.slice(0);
  const movesWithLoc =
    (currentDirection === 'W' || currentDirection === 'S')
    ? number * -1
    : number;

  return (currentDirection === 'N' || currentDirection === 'S')
    ? [currentLocation[0], currentLocation[1] + movesWithLoc, currentDirection]
    : [currentLocation[0] + movesWithLoc, currentLocation[1], currentDirection];
}

const directions = 'L4, L3, R1, L4, R2, R2, L1, L2, R1, R1, L3, R5, L2, R5, L4, L3, R2, R2, L5, L1, R4, L1, R3, L3, R5, R2, L5, R2, R1, R1, L5, R1, L3, L2, L5, R4, R4, L2, L1, L1, R1, R1, L185, R4, L1, L1, R5, R1, L1, L3, L2, L1, R2, R2, R2, L1, L1, R4, R5, R53, L1, R1, R78, R3, R4, L1, R5, L1, L4, R3, R3, L3, L3, R191, R4, R1, L4, L1, R3, L1, L2, R3, R2, R4, R5, R5, L3, L5, R2, R3, L1, L1, L3, R1, R4, R1, R3, R4, R4, R4, R5, R2, L5, R1, R2, R5, L3, L4, R1, L5, R1, L4, L3, R5, R5, L3, L4, L4, R2, R2, L5, R3, R1, R2, R5, L5, L3, R4, L5, R5, L3, R1, L1, R4, R4, L3, R2, R5, R1, R2, L1, R4, R1, L3, L3, L5, R2, R5, L1, L4, R3, R3, L3, R2, L5, R1, R3, L3, R2, L1, R4, R3, L4, R5, L2, L2, R5, R1, R2, L4, L4, L5, R3, L4';

const finalLocation = directions.split(', ').reduce((red, cur, index) => {
  const [direction, ...number] = cur.split('');
  const newDirection = rotate(direction, red[2]);

  return move(number.join('') * 1, newDirection, red);
}, [0, 0, 'N']);

console.log('finalLocation', finalLocation);
console.log('number of blocks', Math.abs(finalLocation[0]) + Math.abs(finalLocation[1]))
