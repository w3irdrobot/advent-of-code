/*
  The first floor contains a thulium generator, a thulium-compatible microchip, a plutonium generator, and a strontium generator.
  The second floor contains a plutonium-compatible microchip and a strontium-compatible microchip.
  The third floor contains a promethium generator, a promethium-compatible microchip, a ruthenium generator, and a ruthenium-compatible microchip.
  The fourth floor contains nothing relevant.
*/
const getInitialState = floors => {
  const seenElements = [];
  const initialState = [];

  input.forEach((floor, level) => {
    const stuff = floor.split('a ');
    const chips = stuff.filter(s => s.includes('microchip'));
    const gens = stuff.filter(s => s.includes('generator'));

    chips.forEach(c => {
      const element = c.split('-')[0];

      if (seenElements.includes(element)) {
        const index = seenElements.indexOf(element);

        initialState[index][1] = level + 1;
      } else {
        const index = seenElements.length;

        seenElements.push(element);
        initialState[index] = [0, level + 1];
      }
    });

    gens.forEach(g => {
      const element = g.split(' ')[0];

      if (seenElements.includes(element)) {
        const index = seenElements.indexOf(element);

        initialState[index][0] = level + 1;
      } else {
        const index = seenElements.length;

        seenElements.push(element);
        initialState[index] = [level + 1, 0];
      }
    });
  });

  return initialState;
};

const getNumberToTop = initialState => {

};

const input = `The first floor contains a hydrogen-compatible microchip and a lithium-compatible microchip.
The second floor contains a hydrogen generator.
The third floor contains a lithium generator.
The fourth floor contains nothing relevant.`.split('\n');
const currentFloor = 0;

const initialState = getInitialState(input);
const numberOfMoves = getNumberToTop(initialState);
