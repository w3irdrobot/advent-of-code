function rotate(rotationDirection, currentDirection) {
  const possibleDirections = ['N', 'E', 'S', 'W'];
  const i = possibleDirections.indexOf(currentDirection);
  const newI = rotationDirection === 'R' ? i + 1 : i - 1
  const wrappedIndex = (newI + possibleDirections.length) % possibleDirections.length;

  return possibleDirections[wrappedIndex];
}

function move(number, currentDirection, currentLocation) {
  const [xCoord, yCoord] = currentLocation;
  const curLoc = currentLocation.slice(0);
  const movesWithLoc =
    (currentDirection === 'W' || currentDirection === 'S')
    ? number * -1
    : number;

  return (currentDirection === 'N' || currentDirection === 'S')
    ? [xCoord, yCoord + movesWithLoc, currentDirection]
    : [xCoord + movesWithLoc, yCoord, currentDirection];
}

function getMiddleSteps(oldLocation, newLocation) {
  const locations = [];

  // Moving east
  if (oldLocation[0] < newLocation[0])
    for (let i = oldLocation[0] + 1; i <= newLocation[0]; i++)
      locations.push(`${i},${oldLocation[1]}`);

  // Moving west
  if (oldLocation[0] > newLocation[0])
    for (let i = oldLocation[0] - 1; i >= newLocation[0]; i--)
      locations.push(`${i},${oldLocation[1]}`);

  // Moving north
  if (oldLocation[1] < newLocation[1])
    for (let i = oldLocation[1] + 1; i <= newLocation[1]; i++)
      locations.push(`${oldLocation[0]},${i}`);

  // Moving south
  if (oldLocation[1] > newLocation[1])
    for (let i = oldLocation[1] - 1; i >= newLocation[1]; i--)
      locations.push(`${oldLocation[0]},${i}`);

  return locations;
}

const directions = 'L4, L3, R1, L4, R2, R2, L1, L2, R1, R1, L3, R5, L2, R5, L4, L3, R2, R2, L5, L1, R4, L1, R3, L3, R5, R2, L5, R2, R1, R1, L5, R1, L3, L2, L5, R4, R4, L2, L1, L1, R1, R1, L185, R4, L1, L1, R5, R1, L1, L3, L2, L1, R2, R2, R2, L1, L1, R4, R5, R53, L1, R1, R78, R3, R4, L1, R5, L1, L4, R3, R3, L3, L3, R191, R4, R1, L4, L1, R3, L1, L2, R3, R2, R4, R5, R5, L3, L5, R2, R3, L1, L1, L3, R1, R4, R1, R3, R4, R4, R4, R5, R2, L5, R1, R2, R5, L3, L4, R1, L5, R1, L4, L3, R5, R5, L3, L4, L4, R2, R2, L5, R3, R1, R2, R5, L5, L3, R4, L5, R5, L3, R1, L1, R4, R4, L3, R2, R5, R1, R2, L1, R4, R1, L3, L3, L5, R2, R5, L1, L4, R3, R3, L3, R2, L5, R1, R3, L3, R2, L1, R4, R3, L4, R5, L2, L2, R5, R1, R2, L4, L4, L5, R3, L4'.split(', ');

let currentLocation = [0, 0, 'N'];
let visitedLocations = [];

for (let i = 0; i < directions.length; i++) {
  const [direction, ...number] = directions[i].split('');
  const newDirection = rotate(direction, currentLocation[2]);
  const newLocation = move(number.join('') * 1, newDirection, currentLocation);

  visitedLocations = visitedLocations.concat(getMiddleSteps(currentLocation, newLocation))

  currentLocation = newLocation;
}

const twiceVisited = visitedLocations.find((loc, i, locations) =>
  locations.indexOf(loc, i + 1) > -1
);
const [xCoord, yCoord] = twiceVisited.split(',');
console.log('number of blocks', Math.abs(xCoord * 1) + Math.abs(yCoord * 1));
