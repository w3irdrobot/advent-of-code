const input = `cpy 1 a
cpy 1 b
cpy 26 d
jnz c 2
jnz 1 5
cpy 7 c
inc d
dec c
jnz c -2
cpy a c
inc a
dec b
jnz b -2
cpy c b
dec d
jnz d -6
cpy 13 c
cpy 14 d
inc a
dec d
jnz d -2
dec c
jnz c -5`;

const getRegisterIndex = letter =>
  letter.charCodeAt(0) % 97;

const processAssembunny = instructions => {
  const registers = [0, 0, 0, 0];
  const ins = instructions.split('\n');
  let cursor = 0;

  while (cursor < ins.length) {
    const [action, ...rest] = ins[cursor].split(' ');

    switch (action) {
      case 'cpy':
        const [v1, r1] = rest;
        const moveToRegIndex = getRegisterIndex(r1);
        const moveValue = parseInt(v1, 10);

        if (isNaN(moveValue)) {
          const moveFromRegIndex = getRegisterIndex(v1);

          registers[moveToRegIndex] = registers[moveFromRegIndex];
        } else {
          registers[moveToRegIndex] = moveValue;
        }

        cursor++;
        break;
      case 'inc':
        const r2 = rest[0];
        const i1 = getRegisterIndex(r2);

        registers[i1]++;
        cursor++;
        break;
      case 'dec':
        const r3 = rest[0];
        const i2 = getRegisterIndex(r3);

        registers[i2]--;
        cursor++;
        break;
      case 'jnz':
        const [r4, v2] = rest;
        const registerValue = parseInt(r4, 10);
        const jumpValue = parseInt(v2, 10);

        if (isNaN(registerValue)) {
          const i3 = getRegisterIndex(r4);

          if (registers[i3]) {
            cursor += jumpValue;
          } else {
            cursor++;
          }
        } else {
          if (registerValue) {
            cursor += jumpValue;
          } else {
            cursor++;
          }
        }

        break;
    }
  }

  return registers;
}

const finalRegisters = processAssembunny(input);

console.log('the final value in register "a" is', finalRegisters[0]);
