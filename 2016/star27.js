const crypto = require('crypto');

const getHash = str =>
  crypto
    .createHash('md5')
    .update(str)
    .digest('hex');

const isNotExpired =
  currentIndex =>
    ({ index }) =>
      currentIndex < index + 1001

const hasStringOfFive = (needle, haystack) =>
  !!haystack.match(`${needle}{5}`);

const isATriple = str =>
  str[0] === str[1] && str[0] === str[2];

const getKeys = salt => {
  const validKeys = [];
  let possibleKeys = [];
  let currentIndex = 0;

  while (validKeys.length < 64) {
    const hash = getHash(`${salt}${currentIndex}`);

    // prune old ones
    possibleKeys = possibleKeys.filter(isNotExpired(currentIndex))

    possibleKeys.slice(0).forEach(({ index, letter }, i) => {
      if (hasStringOfFive(letter, hash)) {
        validKeys.push(possibleKeys[i]);
        possibleKeys.splice(i, 1);
      }
    });

    for (let i = 0; i < hash.length - 2; i++) {
      if (isATriple(hash.slice(i, i + 3))) {
        possibleKeys.push({ index: currentIndex, letter: hash[i] });
        break;
      }
    }

    currentIndex++;
  }

  return validKeys;
};

// const input = 'ahsbgdzn';
const input = 'abc';
const keys = getKeys(input);

console.log('last index was', keys);
