'use strict';

const crypto = require('crypto');
const input = 'yzbqklnj';
let counter = 0;

const getHash = function(plain) {
    let md5sum = crypto.createHash('md5');
    md5sum.update(plain);
    return md5sum.digest('hex');
};

while (getHash(`${input}${counter}`).substr(0, 5) !== '000000') counter++;

console.log(counter);