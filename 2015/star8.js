'use strict';

const cluster = require('cluster');
const crypto = require('crypto');
const input = 'yzbqklnj';
let counter = 0;

if (cluster.isMaster) {
    for (let i = 0; i < require('os').cpus().length; i++) {
        let worker = cluster.fork();

        worker.on('message', function(m) {
            if (m.hashGood) {
                console.log('Final output', m.counter);
                if (!worker.suicide) {
                    cluster.disconnect(function() {
                        console.log('all clusters killed. shutting down');
                    });
                }
            } else {
                worker.send({ input: input, counter: counter++});
            }
        });

        worker.send({ input: input, counter: counter++});
    }
}

if (cluster.isWorker) {
    const getHash = function(plain) {
        let md5sum = crypto.createHash('md5');
        md5sum.update(plain);
        return md5sum.digest('hex');
    };

    process.on('message', function(m) {
        let hashGood = false;
        if (getHash(`${m.input}${m.counter}`).substr(0, 6) === '000000') {
            hashGood = true;
        }

        process.send({
            input: m.input,
            counter: m.counter,
            hashGood: hashGood
        });
    });
}