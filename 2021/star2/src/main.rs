use itertools::Itertools;
use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    let fd = File::open("input.txt").expect("input file");
    let mut reader = BufReader::new(fd)
        .lines()
        .map(|n| n.unwrap().parse::<i32>().unwrap())
        .tuple_windows::<(_, _, _)>();
    let first = reader.next().unwrap();
    let mut prev = first.0 + first.1 + first.2;
    let mut count = 0;

    for (i, j, k) in reader {
        let sum = i + j + k;
        if prev < sum {
            count += 1;
        }
        prev = sum;
    }

    println!("{} larger measurements", count);
}
