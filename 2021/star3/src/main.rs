use itertools::Itertools;
use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    let file = File::open("input.txt").expect("input file");
    let reader = BufReader::new(file).lines().map(|s| {
        s.unwrap()
            .split(' ')
            .map(str::to_owned)
            .collect_tuple::<(_, _)>()
            .unwrap()
    });
    let (mut x, mut y) = (0, 0);

    for (command, amount) in reader {
        let amount = amount.parse::<i32>().unwrap();
        match command.as_ref() {
            "forward" => x += amount,
            "down" => y += amount,
            "up" => y -= amount,
            _ => unimplemented!(),
        }
    }

    println!("x * y = {}", x * y);
}
