use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    let fd = File::open("input.txt").expect("input file");
    let mut reader = BufReader::new(fd)
        .lines()
        .map(|n| n.unwrap().parse::<i32>().unwrap());
    let mut prev = reader.next().unwrap();
    let mut count = 0;

    for n in reader {
        if prev < n {
            count += 1;
        }
        prev = n;
    }

    println!("{} larger measurements", count);
}
