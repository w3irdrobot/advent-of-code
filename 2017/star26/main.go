package main

import (
	"fmt"
	"strconv"
	"strings"
	"time"
)

var input = `0: 4
1: 2
2: 3
4: 4
6: 8
8: 5
10: 8
12: 6
14: 6
16: 8
18: 6
20: 6
22: 12
24: 12
26: 10
28: 8
30: 12
32: 8
34: 12
36: 9
38: 12
40: 8
42: 12
44: 17
46: 14
48: 12
50: 10
52: 20
54: 12
56: 14
58: 14
60: 14
62: 12
64: 14
66: 14
68: 14
70: 14
72: 12
74: 14
76: 14
80: 14
84: 18
88: 14`

func elapsed(what string) func() {
	start := time.Now()

	return func() {
		fmt.Printf("%s took %v\n", what, time.Since(start))
	}
}

func processInput() map[int]int {
	layers := make(map[int]int)

	for _, data := range strings.Split(input, "\n") {
		dataSplit := strings.Split(data, ": ")
		layerNumber, _ := strconv.Atoi(dataSplit[0])
		scannerRange, _ := strconv.Atoi(dataSplit[1])

		layers[layerNumber] = scannerRange
	}

	return layers
}

func calculateScannerLocation(depth int, delay int, scannerRange int) int {
	return (delay + depth) % ((scannerRange - 1) * 2)
}

func main() {
	defer elapsed("main")()
	layers := processInput()
	delay := 0
	madeItThrough := false

outside:
	for !madeItThrough {
		for i := 0; i < 89; i++ {
			scannerRange, ok := layers[i]

			if ok && calculateScannerLocation(i, delay, scannerRange) == 0 {
				delay++
				continue outside
			}
		}

		madeItThrough = true
	}

	fmt.Printf("The delay necessary to safely make it through is %d.\n", delay)
}
