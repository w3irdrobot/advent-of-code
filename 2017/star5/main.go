package main

import (
	"fmt"
	"math"
)

var input = 312051

func main() {
	level := 1
	highNumber := 1

	for highNumber < input {
		highNumber += 8 * level // 8 cubes added per level
		level++
	}

	width := 1 + 2*level
	height := 1 + 2*(level-1)

	if input > highNumber-width { // in bottom side
		middle := highNumber - width/2
		distance := level + int(math.Abs(float64(input-middle)))

		fmt.Printf("The distance is %d.\n", distance)
	} else if input > highNumber-width-height { // in left side
	} else if input > highNumber-width*2-height { // in top side
	} else { // in right side
	}
}
