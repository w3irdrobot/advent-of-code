package main

import (
	"fmt"
	"strconv"
	"strings"
)

const (
	input = "271973-785961"
)

func main() {
	parts := strings.Split(input, "-")
	start, _ := strconv.Atoi(parts[0])
	end, _ := strconv.Atoi(parts[1])
	count := 0

	for i := start; i <= end; i++ {
		if !hasDuplicate(i) || !isIncreasing(i) {
			continue
		}
		count++
	}

	fmt.Printf("the count is %d\n", count)
}

func hasDuplicate(i int) bool {
	prev := i % 10
	rest := i / 10
	matchingPairs := 0

	for rest != 0 {
		current := rest % 10

		if current == prev {
			matchingPairs++
		} else if matchingPairs == 1 {
			return true
		} else {
			matchingPairs = 0
		}

		prev = current
		rest = rest / 10
	}

	return matchingPairs == 1
}

func isIncreasing(i int) bool {
	prev := i % 10
	rest := i / 10

	for rest != 0 {
		current := rest % 10

		if current > prev {
			return false
		}

		prev = current
		rest = rest / 10
	}

	return true
}
