package main

import (
	"fmt"
	"os"

	"gitlab.com/searsaw/advent-of-code/2019/pkg/intcode"
)

const (
	input = "3,8,1001,8,10,8,105,1,0,0,21,34,51,64,73,98,179,260,341,422,99999,3,9,102,4,9,9,1001,9,4,9,4,9,99,3,9,1001,9,4,9,1002,9,3,9,1001,9,5,9,4,9,99,3,9,101,5,9,9,102,5,9,9,4,9,99,3,9,101,5,9,9,4,9,99,3,9,1002,9,5,9,1001,9,3,9,102,2,9,9,101,5,9,9,1002,9,2,9,4,9,99,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,99,3,9,101,1,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,99,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,2,9,4,9,99,3,9,1001,9,1,9,4,9,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,99"
)

func main() {
	possibilities := getPossibleCombos([]int{0, 1, 2, 3, 4})
	largestOutput := 0
	for _, p := range possibilities {
		aprog := intcode.NewProgram(input)
		aOut, err := aprog.Run([]int{p[0], 0})
		if err != nil {
			handleError(err)
		}

		bprog := intcode.NewProgram(input)
		bOut, err := bprog.Run([]int{p[1], aOut})
		if err != nil {
			handleError(err)
		}

		cprog := intcode.NewProgram(input)
		cOut, err := cprog.Run([]int{p[2], bOut})
		if err != nil {
			handleError(err)
		}

		dprog := intcode.NewProgram(input)
		dOut, err := dprog.Run([]int{p[3], cOut})
		if err != nil {
			handleError(err)
		}

		eprog := intcode.NewProgram(input)
		eOut, err := eprog.Run([]int{p[4], dOut})
		if err != nil {
			handleError(err)
		}

		if eOut > largestOutput {
			largestOutput = eOut
		}
	}

	fmt.Printf("the largest output is %d\n", largestOutput)
}

func getPossibleCombos(values []int) [][]int {
	if len(values) == 1 {
		return [][]int{values}
	}

	var combos [][]int

	for i, v := range values {
		var vs []int
		vs = append(vs, values[:i]...)
		vs = append(vs, values[i+1:]...)

		for _, c := range getPossibleCombos(vs) {
			fc := []int{v}
			fc = append(fc, c...)
			combos = append(combos, fc)
		}
	}

	return combos
}

func handleError(err error) {
	fmt.Printf("error looking for the strongest signal: %s", err)
	os.Exit(1)
}
