package intcode

import (
	"strconv"
	"strings"
	"testing"
)

func TestRun(t *testing.T) {
	type args struct {
		memory string
		inputs []int
	}
	tests := []struct {
		name    string
		args    args
		want    int
		wantErr bool
	}{
		{
			"add and multiplies correctly",
			args{"1,9,10,3,2,3,11,0,99,30,40,50", []int{0}},
			3500, true,
		},
		{
			"compares to 8 and returns 0 when input isn't 8",
			args{"3,9,8,9,10,9,4,9,99,-1,8", []int{5}},
			0, false,
		},
		{
			"compares to 8 and returns 1 when input is 8",
			args{"3,9,8,9,10,9,4,9,99,-1,8", []int{8}},
			1, false,
		},
		{
			"output 999 if input is less than 8",
			args{
				memory: "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99",
				inputs: []int{4},
			},
			999, false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := NewProgram(tt.args.memory)
			got, err := p.Run(tt.args.inputs)
			if (err != nil) != tt.wantErr {
				t.Errorf("Run() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Run() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func getMemory(input string) []int {
	parts := strings.Split(input, ",")
	memory := make([]int, len(parts))
	for i, part := range parts {
		n, _ := strconv.Atoi(part)
		memory[i] = n
	}
	return memory
}
