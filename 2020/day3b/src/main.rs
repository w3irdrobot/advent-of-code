const INPUT: &str = ".....##.#.....#........#....##.
....#...#...#.#.......#........
.....##.#......#.......#.......
...##.........#...#............
........#...#.......#.........#
..........#......#..#....#....#
..................#..#..#....##
.....##...#..#..#..#..#.##.....
..##.###....#.#.........#......
#.......#......#......#....##..
.....#..#.#.......#......#.....
............#............#.....
...#.#........#........#.#.##.#
.#..#...#.....#....##..........
##..........#...#...#..........
...........#...###...#.......##
.#..#............#........#....
##.#..#.....#.......#.#.#......
.##.....#....#.#.......#.##....
..##...........#.......#..##.#.
##...#.#........#..#...#...#..#
.#..#........#.#.......#..#...#
.##.##.##...#.#............##..
..#.#..###......#..#......#....
.#..#..#.##.#.##.#.#...........
...#....#..#.#.#.........#..#..
......#.#....##.##......#......
#....#.##.##....#..#...........
...#.#.#.#..#.#..#.#..#.##.....
#.....#######.###.##.#.#.#.....
..#.##.....##......#...#.......
..#....#..#...##.#..#..#..#..#.
.............#.##....#.........
.#....#.##.....#...............
.#............#....#...#.##....
.#.....#.##.###.......#..#.....
.#...#.........#.......#..#....
..#.#..#.##.......##...........
.....##..#..#..#..#.##..#.....#
..##............##...#..#......
...#..#....#..##.....##..#.#...
#.....##....#.#.#...#...#..##.#
#.#..#.........#.##.#...#.#.#..
.....#.#....##....#............
#.......#..#.....##..#...#...#.
.....#.#...#...#..#......#.....
..##....#.#.#.#.#..#...........
##..#...#.........#......#...#.
..#...#.#.#.#..#.#.##..##......
#............###.....###.......
..........#...#........###.....
.......##...#...#...#........#.
.#..#.##.#.....................
.#..##........##.##...#.......#
.......##......#.....#......#..
.##.#.....#......#......#......
#...##.#.#...#.#...............
........#..#...#.##.......#....
...................#...#...##..
...#...#.........#.....#..#.#..
.###..#........#..##.##..#.##..
#...#.....#.....#.....#..#..#..
###..#.....#.#.#.#......#....#.
#........#....##.#...##........
.#.#..##........##....##.#.#...
#...#....#.###.#.#.........#...
...#...##..###.......#.........
......#....#..##..#.....#.#....
........#...##...###......##...
..........##.#.......##........
...#....#......#...##.....#....
###.#.....#.#..#..#....#...#..#
.#.....#.#....#...............#
..#....#....####....###....#.#.
....##........#..#.##.#....#...
.......##...#...#..#....####...
#...##.#......##...#..#........
..##..#.##....#.......##.#.#...
..#.#...............#...#.#....
....#.....#.#.....#.##.......#.
...#.#..##.#.#..............##.
..#.....#...#.............#.##.
##..#.#...#........#..#.....##.
...........##...#.#.###...#....
...#.#.#..#..................#.
.#...##.............#...#......
..#..#...#.#.......#...#.....#.
..##.......#.#.................
.##..#........###.....#....#.##
......#..###.......#....##....#
....#.....#.................#..
........#...#...#..............
...#..#.###.......#..#.#.#.##..
..#...#.....#....#.........#...
...#.............#........###..
......#..............#......#..
#..#...........#...#..........#
...##...#.###..#...#.....#.#...
....#..##......#.......##......
....#....##.#...#.#..#....#...#
.#...........#..#....##...#..##
..#.#.................###.#...#
..#.#.#...##...........#.......
..........#..##...#.#..##....##
........#........#.##..#.#...#.
.....#...##.......##......#...#
....#...#..#..#.....#..........
.#..#......#..#..#..###.......#
.##..........#...#...#.#.....##
..#..........#.#.#...###.......
....#................#...##....
.##..#....#..........#.#.#.....
..##...#.#........#.....#.##...
....####.....#..#.........##..#
......#.........#...#..........
....#...................#..##..
.##....#.#.........#....#...#..
....##...##.....#..####........
..##.#....#.#.......##...#.....
#...#.#.#...#..#..##.....#.....
#..................###.....#...
#.#.....#.......#.#...###.#....
.#..#....#............#........
#.#....#..#.#...............#..
..#..#..#.............#......#.
..#.......##...................
.#....#.........#....#.#.#..#..
....#....#..#...............#..
......#..#..##......#.........#
..#.##........##......#..#..#.#
#.....#.#....#.........##...#..
###..............#....###...##.
....#..##......#.......##......
......#...#.##......##....#..#.
..........#....#..##.......#..#
.#..#...##..#...........#..#..#
.....#....#...#..###...###....#
.#####..#...#.#.#..#.#.###...##
..##............##.#...#.##...#
.##..#...#...#....##.#..#..##..
.#....#...#............##..#...
.#.#......#....#....#..##..##..
.........#...#.......#.##..#...
#.........#.....##.....#..#..#.
...##.#...#...#..#..#....##..##
.#............#...#....##......
..#...#.##.........#.#......#.#
....#.##........#.........#..##
#.........#......#.#......#..#.
........#.#.......#.#........#.
..#..........##.#...#..#.#.....
..#...#....#...#...#..#.#..#.#.
.#.........#....#..#####..#....
#.#....#.#.###...#.............
..##...........##......##......
#.....#..#....#...............#
...#.#..#....##......#...##....
...#........#.....#...#..#.....
.#......##.........#......#....
..#..###.##...#.#.....#........
.............#......#..#.......
..#...............#.#...#..#..#
.......#..#...#.#####......#..#
.........#.....#...............
##........#............#.#.....
.#...#.....#..#..#...#....#...#
..#....#....##......##.....#.#.
#...##..##......#...#....#.....
....#.#.#.....###....##.##....#
..........##...##.......#......
..#.......#...##.#....##.##....
....#........................#.
...#...#.#.##...#.....#...#..#.
.#....##..#..#..........##..##.
.#.....#..#...#.##.....#.......
.#.##...#.#..#.....##....#...#.
.##...#........##....#..#......
.....#........#..........#.#..#
....#..##.......#..#.....#.....
...........#...#........#.##..#
.....#..#....#..#.#.....#....##
.....#....#.##.#..##...........
...##.......##.........#.......
...............##..#....#.#....
.......###..#........#..####.##
.......#.##...#.#....#.####....
....#...............#..........
##.#.......#.....#......#...#..
......##.....#....#.....#..#..#
.....#...##.............#......
#.#.##.#.....#..#........#.....
......##....#..#........#......
............#........#..#.#....
##.......#......#...####..#.##.
..##..#...#.............#.##...
.....#..##......#.##......###..
............#........#........#
#.#.#.#...#.#.....#.........#..
.........#...............#.....
.............###.#.......#....#
###.##..#..#..........#....#...
#......#...#..#..#.....#.##....
............#....#....#..#.....
..#.#....#...#......#.#..#..##.
...#........................#..
#.#...#..........#......#.#....
.........#................#...#
##.....#....#........##.......#
#...##........#...#...........#
...#...#..........##.......#.#.
..#.#.#....#......##...........
...#.#...#.##.#..#.#.##........
#....##.....###..#.......#.....
###.....#.#.#...#..#.........##
..#......#..###...#.#.#.....#.#
.#....#.....#............#..##.
....#....##..........#.....##..
#...........#....#...#..#...##.
..#.......#.....#..........#...
.#..#................#......#..
..#......#.#...#..#.#....#....#
...#..#...###..#..##....#.#....
..#..............#.....#.......
...#.#...#.........#.#.........
##......##...........##.#.##..#
..#..##..#....#.#......#.#...##
...#.###....###...#.....#......
#.#................#......#....
..#.....#.....#....##.......#..
.#.#...............##..#.......
...#....#.......#.#.....##..#..
.........#....#.......#.#...##.
#....#......##.#.........##...#
#.............#..##.#.#..##....
...#....#..#...#....#.#.#.#...#
.#....#....#..##.....#.#...###.
##............#.#...##.#..#.#..
##.#....##.....#..#..###....#..
##....#................##......
...##..#...#..###....#.....##..
.#...##......#..#.#.....#...#..
..##......##...#.##.......#....
......#.....#.....##........#.#
##....#...........#............
#.......#....#..#.##..##.#..#..
.#....##.#.....#..#..#.........
.#....#.#.#...#.....##.....#.#.
.......##.#.#........#......##.
##........#.##.......#...#..#..
...###..##....#.#....#.#.......
......#.......#...##.....#...#.
..#......##.#......#.....#.....
.....#.....###...#.............
#...#.#...#...#..#......#......
#.....#.......###.#....###.#...
...#.......#....####....##..#..
#.#.....#....#........#.......#
.........#.......#......#.#...#
..##....#.....##...............
..........#..#.#..#......#.....
..................##...##.#....
........#.......#...#..#.#.#...
.....#.#..##..#..#.#..#.......#
.....#........#..#..#....#....#
##............#..#..#...#....#.
.....#....................##..#
........##.#....###............
##.......#.##................#.
.....###.#..#..#...#....###.##.
.#......#.#....#.....##.#......
...##......##.........#...#....
....####..............#........
#...#.#..##..##.........##.....
......#......#....#..#.........
#.....#.....#.##...............
..#.##..#...##.#.####..#....###
#..#......#....#.##..##...#.#..
#....#.......#.....#.....#.#...
##.......#.....##...#.....#....
...#...##..........#..##..##..#
.###..#..##...#....#...#..#....
......##..###.......###...#....
....#...#.#.......#.##...##..##
#.#......#..##.#.#..#..#..#....
......#........#.......#.......
..........#.#.....##...........
......#..#........#..#.#..###..
##..#.............##..#........
.........#....#.....#.........#
.....#..##...#..#..##.##......#
###..#...........#.......#....#
...............#....#.#........
.##.#...#.#........##....#.....
.##.###...##..###....#...#...#.
.##..#....#.#.#...#.#.#.#...#..
.###.#...#.......#....#..#.....
..#..#.#.#.#........#.....##...
.#.......#.#...#.#...........##
...#.....##....#.....##...#....
................#.....####...#.
.#.#......#.......##...#.##....
.###.........#.#......#..#.#...
#......#...#....#..##.......#..
.##..#....#..#...........#...#.
.#...#.......##........#.##....
..#...........#...##...........
.....##....##......#....#..#...
#......#.#...#.##.#...##....#..
#....................#...##...#
..#............#........#......
.............#.........##.....#
...#...#......##.#...#...#.#...
..#...#.#.................#....
....##...#....#...###.##......#
...#....#...#..#...#....#.....#
...##.#........#..#.........#..
..##.....#..##...#.....##...#..
#.........#.#.#...#......#...#.
#.#...........#...#..#..#..##..
..#..#..##....#..........#.###.
.....#..#....#.#...#...#..#..#.
###.....#..#.................#.
.#..##.##.#......#....##..#....
";

fn main() {
    let input = INPUT.to_string();
    let mut total: i64 = 1;
    for slope in [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)].iter() {
        let mut x = 0;
        let mut trees = 0;

        for line in input.lines().skip(slope.1).step_by(slope.1) {
            let len = line.len();

            x += slope.0;
            if x >= len {
                x %= len;
            }

            let point = line.chars().nth(x).unwrap().to_string();
            if point == "#" {
                trees += 1;
            }
        }

        total *= trees;
    }

    println!("{:?}", total);
}
