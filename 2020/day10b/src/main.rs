use std::collections::HashMap;

const INPUT: &str = "47
61
131
15
98
123
32
6
137
111
25
28
107
20
99
36
2
97
88
124
138
75
112
52
122
78
46
110
41
64
63
16
93
104
105
91
27
45
119
14
1
65
62
118
37
79
77
19
71
35
130
69
5
44
9
48
125
136
103
140
53
126
106
55
129
139
87
68
21
85
76
31
113
12
100
24
96
82
13
70
72
86
26
117
58
132
114
40
54
133
51
92
";

fn main() {
    let mut nums = INPUT
        .lines()
        .map(|x| x.parse::<i32>().unwrap())
        .collect::<Vec<i32>>();
    let mut cache = HashMap::new();

    nums.sort_unstable();
    nums.insert(0, 0);
    nums.push(nums[nums.len()-1]+3);
    println!("num of paths: {:?}", get_path_number(&nums, 0, &mut cache));
}

fn get_path_number(nodes: &[i32], start: usize, cache: &mut HashMap<i32, usize>) -> usize {
    if start == nodes.len() - 1 {
        return 1;
    }

    let mut index = start + 1;
    let mut paths = 0;

    while index < nodes.len() && nodes[index] <= nodes[start] + 3 {
        paths += match cache.get(&nodes[index]) {
            Some(p) => *p,
            None => get_path_number(nodes, index, cache),
        };
        index += 1;
    }

    cache.insert(nodes[start], paths);
    paths
}
